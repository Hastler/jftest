'use strict';

angular.module('jfTest').controller('generalController',
 ['$scope',
   function($scope) {

     $scope.tab.current = 0;

     $('#dtPicker').datetimepicker({
           defaultDate: $scope.newIncident.dateTime
       });

     $scope.$watch('newIncident.well', function(well) {
           $scope.newIncident.region = well.region;
           $scope.newIncident.state = well.state;
           $scope.newIncident.fieldOffice = well.fieldOffice;
     });

     $scope.$watch('newIncident.companyOfReporter', function(companyName) {
       $scope.newIncidentForm.companyOfReporter.$setValidity('required',
         companyName !== null);
     });
   }
 ]
);