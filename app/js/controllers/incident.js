'use strict';

angular.module('jfTest').controller('incidentController',
  ['$scope',
    function ($scope) {

      $scope.tab = {
        current: 0
      };

      $scope.icd = {
        companies: [
          'U.S. Well Services',
          'American Well & Pump',
          'Moss Well Drilling Inc.',
          'RIGZONE',
          'Aztec Drilling'],
        wells: [{
          name: 'Well-01',
          region: 'South',
          state: 'Oklahoma',
          fieldOffice: 'Ringwood'
        }, {
          name: 'Well-02',
          region: 'North',
          state: 'Montana',
          fieldOffice: 'Sidney'
        }, {
          name:'Well-03',
          region: 'North',
          state: 'North Dakota',
          fieldOffice: 'Tioga'
        }],
        severity: [
          'Loss of well control',
          'Fatality(ies)',
          'Hospitalization or medical treatment',
          'Spill offsite > 50 Bbls',
          'Spill to water, any amount',
          'Property damage'
        ]
      };

      $scope.correctiveActions = [];

      $scope.newIncident = {
        dateTime: moment(),
        well: $scope.icd.wells[0],
        noneApply: false
      };
    }
  ]
);