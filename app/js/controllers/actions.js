'use strict';

angular.module('jfTest').controller('actionsController',
  ['$scope',
    function($scope) {

      $scope.tab.current = 1;

      $scope.tableHeaders = [
        'Description of Corrective Action *',
        'Action Taken By (name) *',
        'Company *',
        'Date *',
        ''
      ];

      $scope.companies = ['Pump Repair Company', 'Water Well Drilling', 'Water Pump Service Epping NH'];

      $scope.$watch('editedAction.company', function(companyName) {
        $scope.addEditAction.actionCompany.$setValidity('required',
          companyName !== null && companyName !== undefined);
      });

      $scope.$watch('editedAction.date', function(dateValue) {
        if (dateValue !== undefined) {
          $scope.editedAction.showDate = dateValue.format('L');
        }
      });

      $scope.addAction = function() {
        if ($scope.correctiveActions.length < 5) {
          $scope.editedAction = {
            date: moment().add(1, 'days')
          };
          $scope.addEditAction.$setPristine();
          $scope.showActionEditor = true;
        }
      };

      $scope.editAction = function(index) {
        $scope.editedAction = {
          description: $scope.correctiveActions[index].description,
          company: $scope.correctiveActions[index].company,
          date: $scope.correctiveActions[index].date,
          takenBy: $scope.correctiveActions[index].takenBy,
          index: index
        };
        $scope.editedAction.showDate = $scope.editedAction.date.format('L');
        $scope.showActionEditor = true;
      };

      $scope.saveAction = function() {
        $scope.correctiveActions.push($scope.editedAction);
        $scope.showActionEditor = false;
      };

      $scope.saveEditedAction = function() {
        $scope.correctiveActions[$scope.editedAction.index] = $scope.editedAction;
        $scope.showActionEditor = false;
      };

      $scope.removeAction = function(index) {
        if (index > -1 && $scope.correctiveActions[index] !== undefined) {
          $scope.correctiveActions.splice(index, 1);
        }
      };

      $scope.cancelEditAction = function() {
        $scope.showActionEditor = false;
      }
    }
  ]
);