'use strict';

angular.module('jfTest').controller('submitController',
  ['$scope',
    function($scope) {

      $scope.tab.current = 2;

      $scope.hasSeverity = function() {
        var
          result = false;

        if ($scope.newIncident.severity) {
          $.each($scope.newIncident.severity, function(key, value) {
            if (value) {
              result = true;
            }
          });
        }

        return result;
      };

      $scope.checkValidate = function() {
        return $scope.newIncident.dateTime &&
              $scope.newIncident.reportedBy &&
              $scope.newIncident.companyOfReporter &&
              $scope.newIncident.hLDescription &&
              $scope.newIncident.contactNumber &&
              $scope.newIncident.well &&
              $scope.hasSeverity() &&
              $scope.correctiveActions.length;
      };

      $scope.sendIncident = function() {
        var
          severities = [],
          result,
          cActionsIndex = 1;

        if ($scope.newIncident.noneApply) {
          severities.push('None Apply');
        } else {
          $.each($scope.newIncident.severity, function (severity, isActive) {
            if (isActive) {
              severities.push(severity);
            }
          });
        }

        result = {
          "workflowCreationInformation": {
            "workflowTypeName": "Incident Report",
            "name": "Report - " + moment().format('YYYY-MM-DD')
          },
          "workflowStepUpdateInformation": {
            "stepIdOrName": "Initial Step",
            "fields": [
              {"name": "Date and Time of Incident", "values": [$scope.newIncident.dateTime.format()]},
              {"name": "Reported By", "values": [$scope.newIncident.reportedBy]},
              {"name": "Company of Reporter", "values": [$scope.newIncident.companyOfReporter]},
              {"name": "Contact Number", "values": [$scope.newIncident.contactNumber]},
              {"name": "Supervisor Name", "values": [$scope.newIncident.supervisorName]},
              {"name": "High Level Description of Incident", "values": [$scope.newIncident.hLDescription]},
              {"name": "Well Number", "values": [$scope.newIncident.well.name]},
              {"name": "Region", "values": [$scope.newIncident.well.region]},
              {"name": "State", "values": [$scope.newIncident.well.state]},
              {"name": "Field Office", "values": [$scope.newIncident.well.fieldOffice]},
              {"name": "Incident Severity (Check all that Apply)", "values": severities}
            ]
          }
        };

        $scope.correctiveActions.forEach(function(action) {
          result.workflowStepUpdateInformation.fields.push({
            name: "Description of Corrective Action (" + cActionsIndex + ")",
            values: action.description
          });
          result.workflowStepUpdateInformation.fields.push({
            name: "Action Taken By (name) (" + cActionsIndex + ")",
            values: action.takenBy
          });
          result.workflowStepUpdateInformation.fields.push({
            name: "Company (" + cActionsIndex + ")",
            values: action.company
          });
          result.workflowStepUpdateInformation.fields.push({
            name: "Date (" + cActionsIndex + ")",
            values: action.date.format()
          });
          cActionsIndex++;
        });

        var newWindow = window.open('showjson.html');
        newWindow.data = result;
        /*alert(JSON.stringify(result));*/
      };
    }
  ]
);