'use strict';

angular.module('jfTest').controller('tabsController',
  ['$scope', '$location',
    function (
      $scope, $location
    ) {

      $scope.tabs = [
        {
          title: 'General Information',
          route: '/general'
        },
        {
          title: 'Corrective Actions',
          route: '/actions'
        },
        {
          title: 'REVIEW and SUBMIT',
          route: '/submit'
        }
      ];

      $scope.selectTab = function(index) {
        $scope.tab.current = index;
        $location.path($scope.tabs[index].route);
      }
}]);