'use strict';

angular.module('jfTest').directive('dateValidator', function() {

  return {
    require: 'ngModel',
    restrict: 'A',
    link: function(scope, elm, attrs, ngModelController) {

      var
        dateFormat = attrs.dateFormat || 'datetime';

      elm.parent().datetimepicker({
        pickTime: dateFormat === 'datetime'
      }).on('dp.change', function (e) {
        ngModelController.$setViewValue(e.date);
      });

      ngModelController.$formatters = [function(modelValue) {
        return modelValue === undefined ? undefined : (dateFormat === 'date' ? modelValue.format('L') : modelValue.format('MM/DD/YYYY HH:mm A'));
      }];

      ngModelController.$parsers.unshift(function(viewValue) {
        ngModelController.$setValidity('required', viewValue.isValid());
        return viewValue;
      });
    }
  }
});
