/**
 * Created by andr on 06.01.15.
 */
angular.module('jfTest', ['ngRoute'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/general', {
        templateUrl: 'general',
        controller: 'generalController'
      })
      .when('/actions',  {
        templateUrl: 'actions',
        controller: 'actionsController'
      })
      .when('/submit', {
        templateUrl: 'submit',
        controller: 'submitController'
      })
      .otherwise({
        redirectTo: '/general'
      })
  }]);